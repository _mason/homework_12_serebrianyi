package Main.Exercises1;

public class Conveyor {

    public static void main(String[] args) throws InterruptedException {

        Worker.Worker1 Vasya = new Worker.Worker1();
        Worker.Worker1 Petya = new Worker.Worker1();
        Vasya.setName("Vasya");
        Vasya.start();
        Vasya.join();

        Petya.setName("Petya");
        Petya.start();
    }
}

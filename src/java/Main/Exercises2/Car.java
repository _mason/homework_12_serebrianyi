package Main.Exercises2;

public class Car extends Thread {

    static class CarIs extends Thread {
        @Override
        public void run() {
            print();
            System.out.println("Car with number " + Thread.currentThread().getName() + " is finished!");
        }
    }

    private static void print() {
        System.out.println("Car with number " + Thread.currentThread().getName());
        for (int i = 0; i < 1000; i++) {
            System.out.println("metr" + i + " is done by Car " + Thread.currentThread().getName());
        }
    }
}

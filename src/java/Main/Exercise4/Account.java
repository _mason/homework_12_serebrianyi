package Main.Exercise4;

public class Account {

    private volatile Long balance = 0l;

    public Account(Long balance) {
        this.balance = balance;
    }

    public synchronized void add(Long money) {
        this.balance += money;
        System.out.println("add money");
        System.out.println(money);
        System.out.println(balance);
    }

    public synchronized void withdraw(Long money) {
        this.balance -= money;
        System.out.println("withdraw money");
        System.out.println(money);
        System.out.println(balance);
    }
}

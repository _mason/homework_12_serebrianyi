package Main.Exercises1;

public class Worker extends Thread {

    static class Worker1 extends Thread {
        @Override
        public void run() {
            print();
        }
    }

    private static void print() {
        System.out.println("Worker " + Thread.currentThread().getName() + " started to work!");
        for (int i = 0; i < 100; i++) {
            System.out.println("Detail number " + i + " is done by " + Thread.currentThread().getName());
        }
    }
}

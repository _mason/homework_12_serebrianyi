package Main.Exercises2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CarTotalizator {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String  luckyNumber = reader.readLine();

        for (int i = 0; i < 10; i++) {
            Car.CarIs car = new Car.CarIs();
            car.setName("" + (i+1));
            if (car.getName().equals(luckyNumber) ) car.setPriority(Thread.MAX_PRIORITY);
            else car.setPriority(Thread.MIN_PRIORITY);
            car.start();
        }
    }
}
